import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;

// This class represents a set of sequences.
public class SequenceSet {
	private static BufferedReader fileReader;
	private static LinkedList<Sequence> list = new LinkedList<>();
	private static int k;
	private static int w;
	// Constructor.
	public SequenceSet(){

	}

	// Load sequences from FASTA file.
	public static SequenceSet load(String fileName) throws IOException{
		fileReader = new BufferedReader(new FileReader(fileName));
		String header;
		String seq;
		while (fileReader.ready()) {
				header = fileReader.readLine().replace(">", "");
				seq = fileReader.readLine();
				list.insert(new Sequence(header, seq));
		}
		fileReader.close();
		return new SequenceSet();
	}
	// Return the global usage over all sequences in the set. The word length is k and window step size is w.
	public Usage getUsage(int k, int w) {
		Usage usage = new Usage();
		list.findFirst();
		LinkedList<Pair<String, Integer>> globalUsageList = list.retrieve().getUsage(k, w).getCounts();
		globalUsageList.findFirst();
		usage.add(globalUsageList.retrieve().first, globalUsageList.retrieve().second);
		while (!globalUsageList.last()) {
			globalUsageList.findNext();
			usage.add(globalUsageList.retrieve().first, globalUsageList.retrieve().second);
		}
		while (!list.last()) {
			list.findNext();
			LinkedList<Pair<String, Integer>> globalUsageList1 = list.retrieve().getUsage(k, w).getCounts();
			globalUsageList1.findFirst();
			usage.add(globalUsageList1.retrieve().first, globalUsageList1.retrieve().second);
			while (!globalUsageList1.last()) {
				globalUsageList1.findNext();
				usage.add(globalUsageList1.retrieve().first, globalUsageList1.retrieve().second);
			}
		}
		return usage;
	}
	// Return all sequences in the set in the same order they appear in the file.
	public LinkedList<Sequence> getSequences() throws IOException {
		return list;
	}

	public static void main(String[] args) throws IOException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the file name");
		SequenceSet sequenceSet = SequenceSet.load(reader.readLine());

		//shows the work of a all single sequence
		list.findFirst();
		System.out.println(list.retrieve().getHeader());
		System.out.println(list.retrieve().getSeq());
		System.out.println("Enter length sub-strings sequence");
		k = Integer.parseInt(reader.readLine());
		System.out.println("Enter the step size");
		w = Integer.parseInt(reader.readLine());
		LinkedList<Pair<String, Integer>> usageListFirst = list.retrieve().getUsage(k, w).getCounts();
		usageListFirst.findFirst();
		System.out.print(usageListFirst.retrieve().first + " = " + usageListFirst.retrieve().second + " ");
		while (!usageListFirst.last()) {
			usageListFirst.findNext();
			System.out.print(usageListFirst.retrieve().first + " = " + usageListFirst.retrieve().second + " ");
		}
		System.out.println("\n");
		while (!list.last()) {
			list.findNext();
			System.out.println(list.retrieve().getHeader());
			System.out.println(list.retrieve().getSeq());
			System.out.println("Enter length sub-strings sequence");
			k = Integer.parseInt(reader.readLine());
			System.out.println("Enter the step size");
			w = Integer.parseInt(reader.readLine());
			LinkedList<Pair<String, Integer>> usageListNext = list.retrieve().getUsage(k, w).getCounts();
			usageListNext.findFirst();
			System.out.print(usageListNext.retrieve().first + " = " + usageListNext.retrieve().second + " ");
			while (!usageListNext.last()) {
				usageListNext.findNext();
				System.out.print(usageListNext.retrieve().first + " = " + usageListNext.retrieve().second + " ");
			}
			System.out.println("\n");
		}

		//create global Usage
		System.out.println("Global sequence");
		System.out.println("Enter length sub-strings sequence");
		k = Integer.parseInt(reader.readLine());
		System.out.println("Enter the step size");
		w = Integer.parseInt(reader.readLine());
		LinkedList<Pair<String,Integer>> linkedList = sequenceSet.getUsage(k,w).getCounts();
		linkedList.findFirst();
		System.out.print(linkedList.retrieve().first + " = " + linkedList.retrieve().second + " ");
		while (!linkedList.last()) {
			linkedList.findNext();
			System.out.print(linkedList.retrieve().first + " = " + linkedList.retrieve().second + " ");
		}
		System.out.println("\n");
	}
}
