// This class represents usage data at the sequence or sequence set level.
public class Usage {
	LinkedList<Pair<String, Integer>> list = new LinkedList<>();
//	 Constructor.
	public Usage(){

	}
	// Add a kmer with the corresponding number of occurrences.
	public void add(String kmer, int count) {
		list.insert(new Pair<String, Integer>(kmer, count));
	}
//	Return the number of occurrences of kmer.
	public int getCount(String kmer) {
		int count = -1; // if Sequence not found method return -1
		list.findFirst();
		while (!list.last()) {
			if (list.retrieve().first.equals(kmer)) {
				count = list.retrieve().second;
			}
			list.findNext();
		}
		return count;
	}
//  Return all kmers with their count.
	public LinkedList<Pair<String, Integer>> getCounts(){
		return list;
	}
}

