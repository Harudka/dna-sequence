// This class represents a single sequence.
public class Sequence {
	private String header;
	private String seq;

	// Constructor.
	public Sequence(String header, String seq) {
		this.header = header;
		this.seq = seq;
	}

	public String getHeader() {
		return header;
	}

	public String getSeq() {
		return seq;
	}

	// Return the usage for word length k and window step size w.
	public Usage getUsage(int k, int w) {
		Usage usage = new Usage();
		String result = "";
		char[] charArray = seq.toCharArray();
		//create kmers
		for (int i = 0; i < charArray.length; i += w) {
			String chars = "";

			for (int j = i; j < k; j++) {
				chars += String.valueOf(charArray[j]);
			}

			k += w;
			result += chars + " ";
			if (k > charArray.length) break;
		}
		//set the kmers, count and add to usage
		String[] res = result.split(" ");
		for (int i = 0; i < res.length; i++) {
			int count = 1;
			for (int j = i+1; j < res.length; j++){
				if (res[i].equals(res[j])) {
					res[j] = "remove";
					count++;
				}
			}
			if (!res[i].equals("remove")) {
				usage.add(res[i], count);
			}
		}
		return usage;
	}
}
